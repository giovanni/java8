package example;
import example.examples.Example;
import example.examples.ParallelTest;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Criado por Giovanni Silva <giovanni@pucminas.br>.
 * Date: 14/04/14
 */
@ApplicationScoped
public class MainApplication implements Runnable {
    @Inject
    @Example
    Instance<Runnable> exampleInstances;



    @PostConstruct
    public void init(){
        Util.feedBack("CDI enviroment Enabled");
    }
    @Override
    public void run(){
        Util.feedBack("Running Examples");
        exampleInstances.forEach(
                (c) -> {
                    Util.feedBack("Running " + c.getClass().getSimpleName());
                    c.run();
                }
        );
    }
}
