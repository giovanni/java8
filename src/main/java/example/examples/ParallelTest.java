package example.examples;

import example.Main;
import example.Permutations;

import javax.inject.Named;
import java.util.function.Supplier;
import java.util.stream.IntStream;

/**
 * Criado por Giovanni Silva <giovanni@pucminas.br>.
 * Date: 14/04/14
 */
@Named
@Example
public class ParallelTest implements Runnable {
    /**
     * Prints the elapsed time for any function
     * @param func
     * @param <T>
     * @return
     */
    private  <T> T measure(Supplier<T> func) {
        long start = System.nanoTime();
        T result = func.get();
        long elapsed = System.nanoTime() - start;
        System.out.printf("Execution time %dns\n", elapsed);
        return result;
    }

    /**
     * Prints the elapsed time for any function
     * @param runnable
     */
    private  void measure(Runnable runnable) {
        measure(() -> { runnable.run(); return ""; });
    }
    private  int sum = 0; // Warning: data race

    /**
     * Do something Heavy
     */
    private  void heavyComputation() {
        Permutations.of("abcdefgh").forEach(i -> ++sum);
    }

    @Override
    public  void run() {
        System.out.println("Running One computation");
        measure(this::heavyComputation);
        System.out.printf("Sum: %s\n", sum);
        System.out.println("Running 10 Sequential");
        measure(() -> IntStream.range(0, 10).forEach(i -> heavyComputation()));
        System.out.println("Running 10 parallel");
        measure(() -> IntStream.range(0, 10).parallel().forEach(i -> heavyComputation()));
        System.out.printf("Sum: %s\n", sum);
    }
}
