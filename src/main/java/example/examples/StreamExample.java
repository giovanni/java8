package example.examples;

import example.Util;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Criado por Giovanni Silva <giovanni@pucminas.br>.
 * Date: 14/04/14
 */
@Named
@Example
public class StreamExample implements Runnable {

    @Override
    public void run() {
        Util.feedBack("ForEach");
        List<Integer> list = IntStream.range(0, 10).boxed().collect(Collectors.toList());

        list.stream().forEach(System.out::println);
//        list.stream().forEach((i)-> System.out.println(i));
        Util.feedBack("Filter and map");

        list.stream()
                .filter((i) -> {
                    System.out.println("Filtering " + i);
                    return i % 2 == 0;
                })
                .map((i) -> {
                    System.out.println("Processing " +i);
                    return i * i;
                })
                .limit(2)
                .forEach(System.out::println);
    }
}
