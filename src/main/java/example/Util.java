package example;

/**
 * Class for utilities
 * Criado por Giovanni Silva <giovanni@pucminas.br>.
 * Date: 14/04/14
 */
public class Util {
    /**
     * Echo feedback messages in the default System Out
     * @param message
     */
    public static void feedBack(String message){
        System.out.println("-------------------------");
        System.out.println(message);
        System.out.println("-------------------------");
    }
}
