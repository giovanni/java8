package example;

import org.apache.deltaspike.cdise.api.CdiContainer;
import org.apache.deltaspike.cdise.api.CdiContainerLoader;
import org.apache.deltaspike.cdise.api.ContextControl;
import org.apache.deltaspike.core.api.provider.BeanProvider;

import javax.enterprise.context.ApplicationScoped;

/**
 * This project contains examples from Java 8.
 * It also user CDI in the Java SE enviroment with the help of DeltaSpike, to run all examples.
 * Criado por Giovanni Silva <giovanni@pucminas.br>.
 * Date: 14/04/14
 */
public class Main {
    public static void main(String[] args) {
        Util.feedBack("Starting CDI Container");
        // Start the CDI container
        CdiContainer cdiContainer = CdiContainerLoader.getCdiContainer();
        cdiContainer.boot();

        // Starting the application-context allows to use @ApplicationScoped beans
        ContextControl contextControl = cdiContainer.getContextControl();
        contextControl.startContext(ApplicationScoped.class);

        MainApplication main = BeanProvider.getContextualReference(MainApplication.class, false);

        main.run();
        // Stop de CDI container
        cdiContainer.shutdown();
    }
}
