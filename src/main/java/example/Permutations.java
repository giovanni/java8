package example;

import java.util.stream.Stream;
import com.google.common.collect.*;

/**
 * Criado por Giovanni Silva <giovanni@pucminas.br>.
 * Date: 14/04/14
 */
public class Permutations {
    public static Stream of(String string){
        return Collections2.permutations(Lists.charactersOf(string)).stream();
    }
}
